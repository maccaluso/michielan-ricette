const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const shell = electron.shell
const globalShortcut = electron.globalShortcut
const ipcMain = electron.ipcMain

let client
let mainWindow, editWindow

if( process.env.NODE_ENV === 'dev' )
{
  client = require('electron-connect').client
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
function createWindow () {
  
    if( process.env.NODE_ENV === 'dev' )
    {
      mainWindow = new BrowserWindow({
        x: 50,
        y: 50,
        width: 940,
        height: 960,
        frame: false
      })

      mainWindow.loadURL(`file://${__dirname}/app/index.html`)

      mainWindow.openDevTools()
    }
    else
    {
      mainWindow = new BrowserWindow({
        x: 300,
        y: 40,
        width: 540,
        height: 960,
        frame: false
      })

      mainWindow.loadURL(`file://${__dirname}/app/index.html`)
    }
    
    mainWindow.on('closed', function () { mainWindow = null })

    mainWindow.focus()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow()

  if( process.env.NODE_ENV === 'dev' )
  {
    client.create( mainWindow, () => {
      console.log('main - electron-connect client created')
    })
  }

  // // Register a 'CommandOrControl+X' shortcut listener.
  const ret = globalShortcut.register('CommandOrControl+F', () => {
    mainWindow.setFullScreen(true)
  })

  const esc = globalShortcut.register('Escape', () => {
    mainWindow.setFullScreen(false)
  })

  const reload = globalShortcut.register('CommandOrControl+R', () => {
    mainWindow.reload()
    if(editWindow)
      editWindow.reload()
  })

  const quit = globalShortcut.register('CommandOrControl+Q', () => {
    app.quit()
  })

  const openEdit = globalShortcut.register('CommandOrControl+O', () => {
    editWindow = new BrowserWindow({
      x: 20,
      y: 40,
      width: 900,
      height: 900
    })

    editWindow.loadURL(`file://${__dirname}/app/edit.html`)

    if( process.env.NODE_ENV === 'dev' )
      editWindow.openDevTools()
    
    editWindow.on('closed', function () { editWindow = null })

    editWindow.focus()
  })
})

app.on('will-quit', () => { globalShortcut.unregisterAll() })

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.