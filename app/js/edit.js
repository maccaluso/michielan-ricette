const appRoot = require('app-root-path')
const $ = require('jquery')
const s = require('form-serializer')

$.serializeObject = s.FormSerializer.serializeObject
$.serializeArray = s.FormSerializer.serializeArray

const Datastore = require('nedb')
let db = new Datastore({ filename: appRoot.path + '/data/michielan-ricette', autoload: true });

$(document).ready(() => {
	db.find({}, (err, docs) => {
		console.log(docs)
		buildContentForm(docs)
	})

	$('body').on('click', '#saveAll', (e) => {
		e.preventDefault();
		let data = []
		
		let formData = $('#mainForm').serializeObject()
		for(let i in formData)
		{
			formData[i].langcode = i
			data.push( formData[i] )
		}

		db.remove({ _id: data[0]._id }, {}, function (err, numRemoved) {
			console.log(numRemoved)

			db.remove({ _id: data[1]._id }, {}, function (err, numRemoved) {
				console.log(numRemoved)

				db.remove({ _id: data[2]._id }, {}, function (err, numRemoved) {
					console.log(numRemoved)

					db.insert(data, function (err, newDoc) {
						$('#savedMsg').fadeIn('fast', function() {
							$('#savedMsg').fadeOut(2000)
							window.location.reload()
						})
					});
				});
			});
		});

	})
})

function buildContentForm(d) {
	for(let i = 0; i < d.length; i++)
	{
		let html = '<hr style="margin-top: 30px;">'
		html += '<h1 class="form-toggle" style="background: grey">' + d[i].langcode + '</h1>'
		html += '<div class="content-form collapsed" id="' + d[i].langcode + 'Container">'
			for(let j in d[i])
			{
				if(j != '_id' && j != 'langcode')
				{
					html += '<hr style="margin-top: 30px;"><h3>' + j + '</h3><hr>'
					
					html += '<div class="form-group">'
						html += '<label>age</label>'
						html += '<input type="text" name="' + d[i].langcode + '[' + j + '][age]" value="' + d[i][j].age.replace(/"/g, "&quot;") + '">'
					html += '</div>'
					html += '<br>'
					
					html += '<div class="form-group">'
						html += '<label>title</label>'
						html += '<input type="text" name="' + d[i].langcode + '[' + j + '][title]" value="' + d[i][j].title.replace(/"/g, "&quot;") + '">'
					html += '</div>'
					html += '<br>'

					html += '<div class="form-group">'
						html += '<label>text</label>'
						html += '<textarea rows="8" name="' + d[i].langcode + '[' + j + '][text]">' + d[i][j].text.replace(/"/g, "&quot;") + '</textarea>'
					html += '</div>'
					html += '<br>'
				}

				if(j == '_id')
				{
					html += '<input type="hidden" name="' + d[i].langcode + '[' + j + ']" value="' + d[i][j] + '">'
				}
			}
		html += '</div>'

		$('#mainForm').append(html)
	}
}