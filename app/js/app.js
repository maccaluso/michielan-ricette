// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

// const {dialog} = require('electron').remote
// const win = require('electron').remote.getCurrentWindow()

const appRoot = require('app-root-path')
const jf = require('jsonfile')
const $ = require('jquery')

const TranslationManager = require('./modules/TranslationManager')

let Datastore = require('nedb')
let db = new Datastore({ filename: appRoot.path + '/data/michielan-ricette', autoload: true });

$(document).ready( () => {
	// let data = jf.readFileSync( appRoot.path + '/data/data.json' )

	// db.insert(data, function (err, newDoc) {   // Callback is optional
	//   // newDoc is the newly inserted document, including its _id
	//   // newDoc has no key called notToBeSaved since its value was undefined
	//   console.log(newDoc);
	// });
	// db.remove({}, {}, (err, numRemoved) => {
	// 	console.log(numRemoved)
	// })

	db.find({}, (err, docs) => {
		// console.log(docs)
		TranslationManager.init( docs )
		TranslationManager.translate()
	})

	// TranslationManager.init( data )
	// TranslationManager.translate()

	let currentTarget;
	let enlarge;
	let direction;

	let resetTimeout, textFadeInTimeout, textFadeOutTimeout;
	let lastDimmed, dimInterval, fadeOutTimeout;

	initRecipeListener();
	dimInterval = setInterval( dim, 5000 )

	function initRecipeListener() {
		direction = 'forward';
		enlarge = false;
		$('body').on('click', '.recipe-container', recipeClickListener);
	}

	function recipeClickListener() {
		$('.recipe-container').removeClass('centered').addClass('vanish');
		$(this).removeClass('vanish').addClass('centered');
		currentTarget = this;
	}

	function dim() {
		let toDim = getRandomElement()
		while(toDim == lastDimmed)
		{
			toDim = getRandomElement()
		}
		lastDimmed = toDim

		$('.recipe-container').eq( toDim ).find('.front').find('.fader').addClass('faded');
	}

	function getRandomElement() {
		let randomIndex = Math.floor(Math.random() * 6)
		return randomIndex  
	}

	function getRandomFadeDuration() {
		let randomDuration = Math.floor(Math.random() * 2000)
		return randomDuration  
	}

	$('body').on('transitionend', '.recipe-container', function(e) {
		if(e.target == currentTarget)
		{
			if( e.originalEvent.propertyName == 'width' )
			{
				if( !$(e.target).hasClass('enlarge') ) {
					direction = 'backward';
					$('#navigation').removeClass('visible');
				}
				else
				{
					direction = 'forward';
					$('#navigation').addClass('visible');
				}
			}

			if(direction == 'forward')
			{
				$(this).addClass('flip');
			}
			else
			{
				$(this).removeClass('flip');
			}
		}
	})

	$('body').on('transitionend', '.flipper', function(e) {
		if(e.originalEvent.propertyName == 'opacity')
		{
			return
		}

		if(!enlarge)
		{
			$(this).parent().addClass('enlarge');
			enlarge = true;

			textFadeInTimeout = setTimeout( function(){
				$('.recipe-info').fadeTo(300,1)
				$('#langChange').fadeTo(1000,1)
				clearTimeout( textFadeInTimeout )
			}, 1000 )
		}

		if(e.originalEvent.propertyName == 'transform' && direction == 'backward')
		{
			$(this).parent().removeClass('centered');
			$('.recipe-container').removeClass('vanish');
			$('body').off('click', '.recipe-container', recipeClickListener);
			resetTimeout = setTimeout( initRecipeListener, 1000 )
		}
	})

	$('body').on('transitionend', '.fader', function(e) {
		let el = $(this)
		clearTimeout( fadeOutTimeout )
		fadeOutTimeout = setTimeout( function(){
			el.removeClass('faded')
		}, 1000)
	})

	$('body').on('click', '#home', function(e) {
		$('#langChange').fadeTo(100,0)
		$('.recipe-info').fadeTo(300,0)
		$('#navigation').removeClass('visible');

		textFadeOutTimeout = setTimeout( function(){
			$('.recipe-container').removeClass('enlarge').removeClass('flip').removeClass('centered');
			clearTimeout( textFadeOutTimeout )
		}, 500 )
	})

	$('body').on('click', '#right', function(e) {
		if( $(currentTarget).index() == $('.recipe-container').length-1 ) {
			$(currentTarget).addClass('vanish')
			currentTarget = $('.recipe-container').first()[0]
			$(currentTarget).addClass('centered').addClass('enlarge').removeClass('vanish')
		}
		else
		{
			$(currentTarget).addClass('vanish')
			$(currentTarget).next().addClass('centered').addClass('enlarge').removeClass('vanish')
			currentTarget = $(currentTarget).next()[0]
		}
	})
	$('body').on('click', '#left', function(e) {
		if( $(currentTarget).index() == 0 ) {
			$(currentTarget).addClass('vanish')
			currentTarget = $('.recipe-container').last()[0]
			$(currentTarget).addClass('centered').addClass('enlarge').removeClass('vanish')
		}
		else
		{
			$(currentTarget).addClass('vanish')
			$(currentTarget).prev().addClass('centered').addClass('enlarge').removeClass('vanish')
			currentTarget = $(currentTarget).prev()[0]
		}
	})

	$('body').on('click', '.lang-switch', function(e) {
		TranslationManager.lang = $(e.target).data().lang
		TranslationManager.translate()
		$('.lang-switch').removeClass('toHide');
		$(e.target).addClass('toHide');
	})
})