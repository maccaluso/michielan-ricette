const {ipcRenderer} = require('electron')

const appRoot = require('app-root-path')
const jf = require('jsonfile')
const $ = require('jquery')

const CalibrationManager = require('./CalibrationManager')
// const VideoManager = require('./VideoManager')

class UIManager {
	constructor() {
		this.data = null
		this.container = $('#appContainer')
	}

	initUI(data, calib) {
		this.data = data
		// let size = CalibrationManager.size

		for(let i = 0; i < data.length; i++ )
		{
			let destX, destY, size

			if(calib[i].top)
				destY = calib[i].top
			else
				destY = 100 + Math.round( Math.random() * ( $(window).height() - 200 ) )

			if(calib[i].left)
				destX = calib[i].left
			else
				destX = 100 + Math.round( Math.random() * ( $(window).width() - 200 ) )

			if(calib[i].size)
				size = calib[i].size
			else
				size = CalibrationManager.size
			
			let className
			i == 0 ? className = 'selected' : className = ''

			let DOMElement = `<div 
								class="active-area ${className}" 
								id="${data[i].id}" 
								style="left: ${destX}px; top: ${destY}px; width: ${size}px; height: ${size}px;"
								>
									${i}: ${data[i].id}
								</div>`
								
			this.container.append( DOMElement )
		}

		this.addListeners()
	}

	addListeners() {
		$('body').on('keyup', (e) => {
			console.log( e.which );
			switch( e.which )
			{
				case 9:
					CalibrationManager.selectActiveArea()
					break;
				case 37:
					CalibrationManager.moveSelectedActiveArea( 'left', e.shiftKey )
					break;
				case 38:
					CalibrationManager.moveSelectedActiveArea( 'up', e.shiftKey )
					break;
				case 39:
					CalibrationManager.moveSelectedActiveArea( 'right', e.shiftKey )
					break;
				case 40:
					CalibrationManager.moveSelectedActiveArea( 'down', e.shiftKey )
					break;
				case 67:
					CalibrationManager.toggleCalibrationMode()
					break;
				case 77:
					CalibrationManager.scale( 'down', e.shiftKey )
					break;
				case 83:
					CalibrationManager.saveCalibration()
					break;
				case 88:
					CalibrationManager.resetSelected()
					break;
				case 80:
					CalibrationManager.scale( 'up', e.shiftKey )
					break;
			}
		})

		$('body').on('click', '.active-area', (e) => {
			let indexToPlay = $(e.target).index()
			let videoFile = this.data[indexToPlay].file

			ipcRenderer.send('play', videoFile)
			$('#trigger')[0].play();
		})
	}
}

module.exports = new UIManager()