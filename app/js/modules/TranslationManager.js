const appRoot = require('app-root-path')
const jf = require('jsonfile')
const $ = require('jquery')

class TranslationManager {
	constructor() {
		this.lang = 'EN'
	}

	init( data ) { 
		this.data = data 

		let html = ''
		let hideClass
		let initialShift

		for (let i = 0; i < this.data.length; i++) {
			
			this.data[i].langcode == this.lang ? hideClass = 'toHide' : hideClass = ''
			this.data[i].langcode.charAt(0) == 'I' ? initialShift = 3 : initialShift = 0

			html += '<div class="nav-control lang-switch ' + hideClass + '" data-lang="' + this.data[i].langcode + '" id="' + this.data[i].langcode + '" style="width: ' + 22.22 / (this.data.length - 1) + '%;">'
				html += '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" width="28" height="100" viewBox="0, 0, 28, 100">';
					html += '<g id="Livello_1">';
  						html += '<circle cx="14" cy="50" r="14" fill="white" />';
  						html += '<text x="' + (initialShift + 5) + '" y="55" fill="black">' + this.data[i].langcode + '</text>';
  					html += '</g>';
				html += '</svg>';
				// html += '<span class="langLabel">' + this.data[i].langcode + '</span>'
			html += '</div>'
		}

		$('#home').after( html )
	}

	translate() {
		let langData

		for (let i = 0; i < this.data.length; i++) {
			if( this.data[i].langcode == this.lang )
			{
				langData = this.data[i]
			}
		}
		
		$('.recipe-container').each((i) => {
			let recipeID = $('.recipe-container').eq(i).attr('id')

			let html = ''
			html += '<p class="recipe-age">'
				html += langData[recipeID].age
			html += '</p>'
			html += '<p class="recipe-title">'
				html += langData[recipeID].title
			html += '</p>'
			html += '<p class="recipe-text">'
				html += langData[recipeID].text
			html += '</p>'

			$('.recipe-container').eq(i).find('.back').find('.recipe-info').html( html )
		})
	}
}

module.exports = new TranslationManager()