# Michielan Museum - Recipes Browser

#### License [CC0 (Public Domain)](LICENSE.md)

##INSTALLATION

Open a terminal and, if not installed, install [node.js + npm](https://nodejs.org/), [electron](https://electron.atom.io) and [electron-packager](https://github.com/electron-userland/electron-packager).
Then cd into the root of the cloned repo and type `electron-packager ./`

This should compile an executable for the current platform.
You can also run it without compiling with `npm start` or `npm run startdev` from the root.

##USAGE

The app should start windowed. `Cmd or Ctrl + F` set the app fullscreen. `ESC` to exit fullscreen.
`Cmd or Ctrl + R` reloads and `Cmd or Ctrl + Q` quits.

`Cmd or Ctrl + O` opens a content editing window.